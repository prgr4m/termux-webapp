# termux-webapp

This is a project scaffold used for building web apps on my android phone 
using [termux](https://termux.com). 

- lighttpd
- ruby (cgi)
- sqlite

I've chosen lighttpd over nginx and apache since the configuration is easy, 
is single process and can run cgi as I don't want to run fastcgi processes 
in the background since all I'm really using cgi for is persistence. I've 
chosen ruby because its a versatile language and I don't want to write php 
code anymore. Each app also should have their own database and I don't need 
the overhead of running mysql or postgresql on my phone.

The server is setup to run cgi scripts in any directory, index files can be 
ruby cgi scripts and no need for them to be executable (only on desktop for 
development for Webrick to run cgi script).

## Development Environment

Since I'm not running lighttpd on my desktop, I've decided to go with the 
following setup for termux web development:

- webrick server w/ cgi
- browser-sync w/ proxy back to webrick and for livereload
- rake for project level and ruby tasks
- npm scripts for frontend management
