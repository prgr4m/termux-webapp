var o = require('ospec');
var jsdom = require('jsdom');
var dom = new jsdom.JSDOM("", { 
  // so we can get 'requestAnimationFrame'
  pretendToBeVisual: true,
});

// Fill in the globals mithril needs to operate. also, the first two are 
// often useful to have just in tests
global.window = dom.window
global.document = dom.window.document
global.requestAnimationFrame = dom.window.requestAnimationFrame

// require mithril to make sure it loads properly
require("mithril")

// and now, make sure jsdom ends when the tests end
o.after(() => {
  dom.window.close();
});
