let o = require('ospec');

o.spec('math', () => {
  o('addition works', () => {
    o(1 + 2).equals(3)
  });
});
